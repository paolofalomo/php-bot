<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col" id="testo">

        </div>
    </div>
    <div class="row">
        <div class="col">
            <textarea id="user-message" class="form-control" cols="5" rows="3"></textarea>
            <button id="send" class="btn btn-primary"> invia</button>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script>
    (function ($) {

        function appendMessage(message, user = 'me',time="") {
            if(time){
                time = '<br><small class="text-muted">'+time+'</small>';
            }
            $("#testo").append('<p class="mb-2" data-user="' + user + '">' + message + time+'</p>');

        }
        $.ajax({
            url: "messages.json",

            success: function (msgs) {
                for(var msg in msgs){
                    var t = '';
                    if(msgs[msg].hasOwnProperty('time')){
                       t = msgs[msg].time.date;
                    }
                    appendMessage(msgs[msg].message,msgs[msg].user,t);
                }
            }
        });

        var textArea = $('textarea#user-message');
        textArea.on('keyup', function (e) {
            e.preventDefault();
            if (e.keyCode === 13) {
                $("#send").trigger('click');
            }
        });
        $("#send").on('click', function () {

            var userMessage = textArea.val();
            appendMessage(userMessage);
            textArea.val('');

            setTimeout(function () {
                $.ajax({
                    url: "backend.php",
                    data: {
                        message: userMessage
                    },
                    success: function (result) {
                        appendMessage(result, 'bot')
                    }
                });
            }, 1000);
        });
    })(jQuery)
</script>
<style>
    [data-user="me"] {
        text-align: right;
        color: white;
        background: cornflowerblue;
        padding: 10px;
        border-radius: 0px 20px 0px 20px;
        margin-left: auto;
    }

    [data-user="bot"] {
        /* text-align: right; */
        color: white;
        background: #a7acca;
        padding: 10px;
        border-radius: 20px 0px 20px 0px;
    }

    img.giphy {
        width: 100%;
    }

    [data-user] {
        width: 50%;
    }</style>
</body>
</html>