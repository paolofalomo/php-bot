<?php
/**
 * Created by PhpStorm.
 * User: pol
 * Date: 23/10/2018
 * Time: 00:28
 */

class Giphy
{
    public static function random()
    {
        $json = file_get_contents('https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC');
        $data=json_decode($json);
        $data=collect($data->data);
        return '<img class="giphy" src="'.$data->get('image_url').'">';

    }

}