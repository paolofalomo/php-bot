<?php
/**
 * Created by PhpStorm.
 * User: pol
 * Date: 22/10/2018
 * Time: 23:03
 */

class Request
{

    public static function get($key, $default=null)
    {
        if (isset($_REQUEST[$key])){
            return $_REQUEST[$key];
        } else {
            return $default;
        }
    }
}