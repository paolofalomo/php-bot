<?php
/**
 * Created by PhpStorm.
 * User: pol
 * Date: 22/10/2018
 * Time: 23:55
 */

class Answer
{
    public function __construct($whatIsAsking = '')
    {
        $this->asking = $whatIsAsking;
        $this->possibleAnswers = collect([]);
        $this->registerAnswer('ciao', ['ciao']);
        $this->registerAnswer('tutto bene', ['come va', 'come stai']);
        $this->registerAnswer('io sono bot', ['chiami', 'come']);
        $this->registerAnswer('oggi è sereno', ['tempo', 'oggi']);
        $this->registerAnswer(Giphy::random(), ['giphy']);
    }

    public function registerAnswer($text, $keywords = null)
    {
        $this->possibleAnswers->push([
            'text' => $text,
            'keywords' => $keywords
        ]);
    }

    private function takeRightAnswer()
    {
        foreach ($this->possibleAnswers as $answer) {
            if (str_contains($this->asking, $answer['keywords'])) {
                return $answer['text'];
            }
        }
        return 'non ho capito';
    }

    public function get(){
        return $this->takeRightAnswer();
    }
}