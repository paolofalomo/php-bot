<?php
/**
 * Created by PhpStorm.
 * User: pol
 * Date: 22/10/2018
 * Time: 23:03
 */

class Bot
{

    public $message;
    private $answer;

    public function listen()
    {
        $this->message = Request::get('message', 'silence');
    }

    public function think()
    {
        Log::append($this->message, 'me');
        if ($this->message == 'silence') {
            $this->setAnswer('non ti sento');
        } else {
            $answer = new Answer($this->message);
            $this->setAnswer($answer->get());
        }
    }

    private function setAnswer($answer)
    {
        Log::append($answer, 'bot');
        $this->answer = $answer;
    }

    private function speak($textToSpeech)
    {
        echo $textToSpeech;
    }

    public function giveAnswer()
    {
        $this->speak($this->answer);
    }
}