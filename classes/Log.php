<?php

use Carbon\Carbon;


class Log
{

    public static function file()
    {
        return __DIR__ . '/../messages.json';
    }

    public static function getLog()
    {
        return collect(json_decode(file_get_contents(self::file())));
    }

    public static function append($message, $user = 'me')
    {
        file_put_contents(self::file(), self::getLog()->push([
            'message' => $message,
            'user' => $user,
            'time'=>Carbon::now()
        ])->toJson(JSON_PRETTY_PRINT));
    }
}